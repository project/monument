PREREQUISITES

This module requires Drupal 4.7.

**********************************************************************
This code is very raw. Not recommended for production use.
**********************************************************************

OVERVIEW

The monument module is designed as a photographic database of historic 
monuments. The idea is to add position data to images of a monument around a
compass rose in such a way that the monument.module can recostruct a 
pseudo-3d view.

This first commit to CVS contains known bugs, currently listed here:
http://www.ascentsoftware.biz/?q=node/6#comment-1
The buglist will be moved to the drupal buglist.

INSTALLATION

Activate the module in the usual way, grant user permissions on monument 
and monument image.

USAGE

Add details of one or two historic monuments (map references are UK based 
at the moment)
Ignore 'Add Monument Image' but use 'Add Image' instead to add a few photos 
of your monuments, specifying bearing and image_locations (see below).

Image Locations
ring1 - taken from the centre of the monument facing radially outward
ring2 - taken from outside the monument facing toward the centre
ring3 - horizon view from the centre of the monument (or equivalent radial 
		point outside monument so as to afford a few of horizon) 
ad_hoc - taken from various points to show features of interest
key_img - specify to show this image with monument preview
art_img - image related to monument, taken for artistic reasons only.

EXAMPLE
The content at www.rockstanza.info will be moved to use this module once it 
reaches production standard. At the moment you can visit www.rockstanza.info 
to get an idea of what is intended
AUTHOR

Chris McGinlay (aka grunthus)
chris at ascentsoftware dot org dot uk
